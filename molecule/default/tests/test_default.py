import os
import pytest
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('molecule_group')


@pytest.mark.parametrize("ioc_host", ["host1", "host2"])
def test_ioc_service(host, ioc_host):
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-diskless-ioc-default":
        script = host.file(f"/opt/iocs/{ioc_host}/.config/ioc@.service")
        assert script.exists
        assert script.contains("AssertPathExists=/opt/iocs/%i/st.cmd")


def test_activation_script(host):
    script = host.file(
        "/home/iocuser/.conda/envs/test-IOC/etc/conda/activate.d/ioc_activate.sh"
    )
    assert script.exists
    assert script.contains('export CUSTOM_VAR="test-ioc"')
    assert script.contains('export IOCNAME="test-IOC"')


def test_conda_env(host):
    assert host.file(
        "/home/iocuser/.conda/envs/test-IOC/base/bin/linux-ppc64e6500/pvget"
    ).exists


def test_list_hosts(host):
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-diskless-ioc-default":
        ioc_hosts = ["host1", "host2", "host3"]
        non_ioc_hosts = ["host4"]
    elif host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-diskless-ioc-hosts":
        ioc_hosts = ["host1", "host4"]
        non_ioc_hosts = ["host2", "host3"]
    for ioc_host in ioc_hosts:
        assert host.file(f"/opt/iocs/{ioc_host}").exists
    for non_ioc_host in non_ioc_hosts:
        assert not host.file(f"/opt/iocs/{non_ioc_host}").exists
