# ics-ans-role-diskless-ioc

Ansible role to install diskless IOCs on a NFS server.

Diskless machines shall mount locally the following directories:

- /e3-conda-ifc14xx/conda on /opt/conda (base conda)
- /home/iocuser/.conda on /home/iocuser/.conda (conda envs)
- /opt/iocs/{{ host }} on /opt/iocs
- /export/nonvolatile/{{ ioc.name }} on /opt/nonvolatile/{{ ioc.name }}

## Role Variables

```yaml
ioc_user_name: iocuser
ioc_user_id: "1042"
ioc_group_name: iocgroup
ioc_group_id: "1042"

# comma separated list of hosts to deploy
# If empty, use all hosts of ioc_diskless_group
ioc_diskless_hosts: ""
ioc_diskless_group: mtca-ifc

ioc_git_url: https://gitlab.esss.lu.se
ioc_git_namespace: ioc
ioc_overwrite: false

ioc_iocs_folder: /opt/iocs

ioc_conda_envs_directory: /home/{{ ioc_user_name }}/.conda/envs

# hostname of the nonvolatile server
# If not empty, the directory /export/nonvolatile/{{ ioc.name }} will be created
# on that machine. It should be mounted in the setup script.
# We use "" string by default so that it's easy to disable at host level
ioc_nonvolatile_server: ""
# Local nonvolatile base directory (set as env in activation script)
ioc_nonvolatile_path: /opt/nonvolatile
# Remote nonvolatile base directory
ioc_nonvolatile_server_path: "/export/nonvolatile"
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-diskless-ioc
```

## License

BSD 2-clause
